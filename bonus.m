function bonus(x,y,m)

	n = length(x);

	% Coefficient a
	a = y;
	 
	% Vector with subintervals
	for j = 1:n-1
		h(j) = x(j+1) - x(j);
	end

	f1prim = (y(2) - y(1))/(x(2) - x(1));
	fn = (y(n) - y(n-1))/(x(n) - x(n-1));

	% Matrix A:
	A = zeros(n);
	 
	% Tensional Spline boundary conditions
	A(1,1) = 2*h(1);
	A(1,2) = h(1);
	A(n,n) = 2*h(n-1);
	A(n,n-1) = h(n-1);
	 
	for i = 2:n-1
		A(i,i-1) = h(i-1);
		A(i,i) = 2*(h(i-1)+h(i));
		A(i,i+1) = h(i);
	end
	 
	% Vector z:
	z = zeros(n,1);
	z(1) = 3/h(1)*(a(2) - a(1)) - 3*f1prim;
	z(n) = 3*fn - 3*(a(n) - a(n-1))/h(n-1); 
	for i = 2:n-1
		z(i) = (3/h(i))*(a(i+1)-a(i)) - (3/h(i-1))*(a(i)-a(i-1));
	end
	 
	% Coefficient c, which we find solving the system Ac = z;
	c = inv(A)*z;
	 
	% Coefficient b
	for i = 1:n-1
		b(i) = (1/h(i))*(a(i+1)-a(i)) - (1/3*h(i))*(2*c(i)+c(i+1));
	end
	 
	% Coefficient d
	for i = 1:n-1
		d(i) = (1/(3*h(i))) * (c(i+1)-c(i));
	end
	 
	for i = 1:n-1
		%Building the function
		f = @(x) a(i) + b(i).*(x-x(i)) + c(i).*(x-x(i)).^2 + d(i).*(x-x(i)).^3;
		%Generate equidistant points in the (xi, xi+1) interval
		xi = linspace(x(i),x(i+1),m);
		%Plotting the image
		hold on;
		plot(xi,f(xi),'b');
	end
	 
end