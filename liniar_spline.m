function yi = liniar_spline(x, y, xi)

	%Decide in which interval the generated points are
	[int_num, k] = histc (xi, x);
	n = length (x);
	%We place the last point into the last interval
	k(k==n) = n - 1;
	%Apply the formulas
	a = (xi - x(k)) ./ (x(k+1) - x(k));
	yi = (1-a) .* y(k) + a .* y(k+1);
	
endfunction
