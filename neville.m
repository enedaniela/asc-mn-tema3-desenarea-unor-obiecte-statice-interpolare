function [yi] = neville(x, y, xi)

n = length(x);

for k = 1:length(xi)
    %From the recurent relation, we observ that Pσ+j+k(xi)=yi

    P = zeros(n,n);
    P(:,1) = y(:);
    %Building Pσ that goes through (xi-j,yi-j),...,(xi,yi) points
    for i = 1:n-1
            for j = 1:(n-i)
                P(j,i+1) = ((xi(k)-x(j))*P(j+1,i) + (x(j+i)-xi(k))*P(j,i))/(x(j+i)-x(j));
            end
    end

    yi(k) = P(1,n);

end
