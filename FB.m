function FB()
	%Open the file
	fid = fopen('totti.txt', 'r');
	%Read the number of points that must be represented
	%and the number of given points
	m = fscanf(fid,'%d',1);
	n = fscanf(fid,'%d\n',1);
	for j = 1:n
		%Read the given points
		v = fgetl(fid);
		v = sscanf(v,'%lf')';
		%Divide the points in x and y vectors
		x = v(1:length(v)/2)';
		y = v(length(v)/2 + 1:length(v))';

		bonus(x,y,m);
		
	end	
	%Close the file
	fclose(fid);
end
