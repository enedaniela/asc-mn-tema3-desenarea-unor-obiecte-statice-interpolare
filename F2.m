function F2()
	%Open the file
	fid = fopen('barcelona.txt', 'r');
	%Read the number of points that must be represented
	%and the number of given points
	m = fscanf(fid,'%d',1);
	n = fscanf(fid,'%d\n',1);
	for i = 1:n
		%Read the given points
		v = fgetl(fid);
		v = sscanf(v,'%lf')';
		%Divide the points in x and y vectors
		x = v(1:length(v)/2);
		y = v(length(v)/2 + 1:length(v));
		%Find the interval
		a = x(1);
		b = x(length(x));
		%Generate equidistant points in the (xi, xi+1) interval
		xi = linspace(a,b,100);
		%Generate the yi coordonates
		yi = liniar_spline(x,y,xi);
		%Plotting the image
		hold on;
		plot(xi,yi);
		
		
	end	
	%Close the file
	fclose(fid);
end
